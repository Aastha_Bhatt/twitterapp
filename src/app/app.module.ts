import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { DefaultModule } from './layouts/default/default.module';
import { ClickOutsideModule } from 'ng-click-outside';
import { ConversationModule } from './layouts/conversation/conversation.module';
import {MatDialogModule} from '@angular/material/dialog';
import {MatFormFieldModule} from '@angular/material/form-field';
import {MatInputModule} from '@angular/material/input';
import {MatSelectModule} from '@angular/material/select';
import {MatDatepickerModule, MatDatepickerToggleIcon, MatDatepickerToggle} from '@angular/material/datepicker';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { ProfilePageRoutingModule } from './modules/profile-page/profile-page-routing.module';
import { HomePageComponent } from './modules/home-page/home-page.component';
import { NotificationPageComponent } from './modules/notification-page/notification-page.component';
import { NotificationPageRoutingModule } from './modules/notification-page/notification-page-routing.module';
import { BookmarkPageComponent } from './modules/bookmark-page/bookmark-page.component';
import { LoginComponent } from './modules/login/login.component';
import { QuoteDialogComponent } from './dialog-box/quote-dialog/quote-dialog.component';
import { ReplyDialogComponent } from './dialog-box/reply-dialog/reply-dialog.component';
import { SetupDialogComponent } from './dialog-box/setup-dialog/setup-dialog.component';
import { SignupDialogComponent } from './dialog-box/signup-dialog/signup-dialog.component';
import { TweetDialogComponent } from './dialog-box/tweet-dialog/tweet-dialog.component';
import { FullWidthComponent } from './layouts/full-width/full-width.component';
import { ProfilePageComponent } from './modules/profile-page/profile-page.component';
import { ProfileComponent } from './modules/profile-page/profile/profile.component';
import { AngularFireModule } from '@angular/fire';
import { AngularFireDatabaseModule } from '@angular/fire/database';
import { environment } from '../environments/environment';
import { AngularFirestoreModule } from '@angular/fire/firestore';
import { HomeComponent } from './modules/home-page/home/home.component';
import { EditProfileComponent } from './dialog-box/edit-profile/edit-profile.component';
import { FollowBtnDirective } from './directives/follow-btn.directive';
// import {DpDatePickerModule} from 'ng2-date-picker';
import { DatepickerModule } from 'ng2-datepicker';

@NgModule({
  declarations: [
    AppComponent,
    TweetDialogComponent,
    ReplyDialogComponent,
    QuoteDialogComponent,
    HomePageComponent,
    NotificationPageComponent,
    BookmarkPageComponent,
    SetupDialogComponent,
    SignupDialogComponent,
    FullWidthComponent,
    LoginComponent,
    ProfilePageComponent,
    ProfileComponent,
    EditProfileComponent,
    FollowBtnDirective

    // HomeComponent
  ],
  imports: [
    DatepickerModule,
    BrowserModule,
    AppRoutingModule,
    NotificationPageRoutingModule,
    ProfilePageRoutingModule,
    DefaultModule,
    ConversationModule,
    ClickOutsideModule,
    BrowserAnimationsModule,
    MatDialogModule,
    FormsModule,
    MatFormFieldModule,
    MatInputModule,
    MatSelectModule,
    MatDatepickerModule,
    // MatDatepickerToggleIcon,
    // MatDatepickerToggle,
    ReactiveFormsModule,
    AngularFireModule.initializeApp(environment.firebase),
    AngularFirestoreModule,
    AngularFireDatabaseModule,
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
