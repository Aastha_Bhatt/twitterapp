import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'filter'
})
export class FilterPipe implements PipeTransform {

  // transform(value: any, filterString:string) {
    
  //   if(value.length === 0 || filterString === ''){
  //     console.log(value,'null');
  //     return value;
  //   }

  //   const users = [];
  //   for(const user of value){
  //     console.log(user);
  //     console.log(filterString);
      
  //     if(user == filterString){
  //       console.log(user,'users');
  //       users.push(user);
  //     }
  //   }
  //   return users;
  // }

  transform(items: any[], searchString: string): any[] {
    if (!items) return [];
    if (!searchString) return items;
  
    return items.filter(item => {
      return Object.keys(item).some(key => {
        return String(item[key]).toLowerCase().includes(searchString.toLowerCase());
      });
    });
   }

}
