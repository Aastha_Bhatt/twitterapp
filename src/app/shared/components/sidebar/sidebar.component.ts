import { Component, ElementRef, HostListener, OnInit, ViewChild } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { TweetDialogComponent } from 'src/app/dialog-box/tweet-dialog/tweet-dialog.component';
import { RegisterService } from 'src/app/services/register.service';
import { UserDataService } from 'src/app/services/user-data.service';

@Component({
  selector: 'app-sidebar',
  templateUrl: './sidebar.component.html',
  styleUrls: ['./sidebar.component.css']
})
export class SidebarComponent implements OnInit {

  showMore:boolean=false
  showProfile:boolean=false
  user:any[]
  userName:string
  userId:string
  uid:string
  profilePic:string;


  @ViewChild('more') more:ElementRef;
  @ViewChild('profile') profile:ElementRef;

  constructor(public TweetDialog:MatDialog, public registerService:RegisterService, public userService: UserDataService) { }

  ngOnInit(): void {
    console.log(this.userService.getUserId());
    
    this.registerService.getUser().subscribe(val => {
      this.user = val.map(u=>{
        if(u.payload.doc.id == this.userService.getUserId()){
          this.userName=u.payload.doc.get('userName');
          this.userId=u.payload.doc.get('userId');
          this.profilePic=u.payload.doc.get('profilePic');
          this.uid=u.payload.doc.id;
        }
      })
    });
  }

  profilePage(){
    console.log('hi');
    this.userService.searchUser='';
    this.userService.showProfilePage();
  }


  openDialog(){    
    this.TweetDialog.open(TweetDialogComponent);
  }

  @HostListener('document:click', ['$event'])
  checkProfileClick(event: { target: any; }){
    if(this.profile.nativeElement.contains(event.target)){
      this.showProfile=true;
    }
    else{
      this.showProfile=false;
    }
  }

  hideProfile(){
    this.showProfile=false;
  }

  hideMore(){
    this.showMore=false;
  }

}
