import { trigger, state, style, transition,
    animate, group, query, stagger, keyframes
} from '@angular/animations';

export const SlideInOutAnimation = [
    trigger('valueUpdated', [
        transition(':increment', group([
          query(':enter', [
            style({ opacity: 0, transform: 'translateY(40%)' }),
            animate(200, style({ opacity: 1, transform: 'translateY(0%)' }))
          ], { optional: true }),
          query(':leave', [
            style({ opacity: 1, transform: 'translateY(0%)' }),
            animate(200, style({ opacity: 0, transform: 'translateY(-40%)' }))
          ], { optional: true })
        ])),
        transition(':decrement', group([
          query(':enter', [
            style({ opacity: 0, transform: 'translateY(-40%)' }),
            animate(200, style({ opacity: 1, transform: 'translateY(0%)' }))
          ], { optional: true }),
          query(':leave', [
            style({ opacity: 1, transform: 'translateY(0%)' }),
            animate(200, style({ opacity: 0, transform: 'translateY(40%)' }))
          ], { optional: true })
        ]))
      ]),
      trigger('likeAnimation', [
        transition('void=>*', group([
          query(':enter', [
            style({ opacity: 0 }),
            animate(2000, style({ opacity: 1 }))
          ], { optional: true }),
          query(':leave', [
            style({ opacity: 0 }),
            animate(2000, style({ opacity: 1 }))
          ], { optional: true }),
        ]))
      ]),
      trigger('routeAnimations',[
        transition('*=>*',[
            query(':enter, :leave',[
                style({
                    position:'absolute',
                    top:0,
                    left:0,
                    width:'100%',
                    display:'block',
                    height:'100%',
                    opacity:0,
                })
            ],{optional:true}),
            query(':enter',[
                style({
                    opacity:0 
                })
            ],{optional:true}),
            group([
                query(':leave',[
                    animate(0,
                        style({
                            opacity:0,
                        }))
                ],{optional:true}),
                query(':enter',[
                    style({
                    }),
                    animate(500,
                        style({
                            opacity:1,
                        })
                    )
                ], { optional: true })
            ])
        ])
    ])
]