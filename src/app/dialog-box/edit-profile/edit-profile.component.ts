import { Component, OnInit } from '@angular/core';
import { AngularFirestore } from '@angular/fire/firestore';
import { AngularFireStorage } from '@angular/fire/storage';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatDialogRef } from '@angular/material/dialog';
import { finalize } from 'rxjs/operators';
import { RegisterService } from 'src/app/services/register.service';
import { UserDataService } from 'src/app/services/user-data.service';

@Component({
  selector: 'app-edit-profile',
  templateUrl: './edit-profile.component.html',
  styleUrls: ['./edit-profile.component.css']
})
export class EditProfileComponent implements OnInit {

  editForm: FormGroup;
  profilePic: string;
  bannerPic:string;
  profileUrl:string;
  bannerUrl:string;
  selectedBannerImg;
  selectedImg;
  userName:any[]=[];
  bio:any[]=[];
  location:string;
  website:string;
  userInfo:any[]=[];

  constructor(public fb: FormBuilder,
    public registerService:RegisterService, 
    public userService: UserDataService,
    public dialogRef: MatDialogRef<EditProfileComponent>,
    public db: AngularFirestore,
    private storage: AngularFireStorage) { }

  ngOnInit(): void {
    
    this.registerService.getUser().subscribe(val => {
      val.map(u=>{
        if(u.payload.doc.id == this.userService.getUserId()){
          this.userName.push(u.payload.doc.get('userName'));
          this.bio.push(u.payload.doc.get('bio'));
          this.profilePic=u.payload.doc.get('profilePic');
          this.bannerPic=u.payload.doc.get('bannerPic');
        }
      })
    });
    this.createEditForm();
  }

  createEditForm(){
    this.userInfo.push({
      userName:this.userName,
      bio:this.bio,
    })
    console.log(this.userInfo.length);
    console.log(this.userInfo);
    this.userInfo.forEach(user => {
      console.log(user['bio']);
      
    })
    this.editForm = this.fb.group({
      name:this.fb.control('',Validators.required),
      bio:this.fb.control('',Validators.required),
      location:this.fb.control('',Validators.required),
      website:this.fb.control('',Validators.required),
      birthdate:this.fb.control('',Validators.required),
    })
    this.editForm.patchValue({
      name:this.userName,
      bio:this.bio,
    })
  }

  updateUserData(){
    this.db.collection('user').doc('/'+this.userService.getUserId()).update({
      userName:this.editForm.value.name,
      bio:this.editForm.value.bio,
      location:this.editForm.value.location,
      website:this.editForm.value.website,
      dob:this.editForm.value.birthdate,
      profilePic:this.profileUrl,
      bannerPic:this.bannerUrl,
    })
    .then(()=>console.log('updated'))
    .catch(e => console.log(e))
  }

  closeDialog(): void {
    this.dialogRef.close();
  }

  showProfilePic(event:any){
    if(event.target.files && event.target.files[0]){
      const reader = new FileReader;
      reader.onload = (e:any) => this.profilePic = e.target.result;
      reader.readAsDataURL(event.target.files[0]);
      this.selectedImg = event.target.files[0];
      var profileFilePath = `${this.userService.getUserId()}/profilePic/${this.selectedImg.name.split('.').slice(0,-1).join('.')}_${new Date().getTime()}`;
    const fileRef = this.storage.ref(profileFilePath);
    this.storage.upload(profileFilePath,this.selectedImg)
    .snapshotChanges()
    .pipe(
      finalize(() => {
        fileRef.getDownloadURL().subscribe((url) => {
          this.profilePic = url;
          console.log(url);
          this.profileUrl=url;
          
        })
      })
      ).subscribe();
    }
  }

  showBannerPic(event:any){
    if(event.target.files && event.target.files[0]){
      const reader = new FileReader;
      reader.onload = (e:any) => this.bannerPic = e.target.result;
      reader.readAsDataURL(event.target.files[0]);
      this.selectedBannerImg = event.target.files[0];
      var bannerFilePath = `${this.userService.getUserId()}/bannerPic/${this.selectedBannerImg.name.split('.').slice(0,-1).join('.')}_${new Date().getTime()}`;
    const fileRef2 = this.storage.ref(bannerFilePath);
    this.storage.upload(bannerFilePath,this.selectedBannerImg)
    .snapshotChanges()
    .pipe(
      finalize(() => {
        fileRef2.getDownloadURL().subscribe((url) => {
          this.bannerPic = url;
          this.bannerUrl=url;
        })
      })
      ).subscribe();
    }
  }

}
