import { Component, ElementRef, HostListener, OnInit, ViewChild } from '@angular/core';
import { AngularFirestore } from '@angular/fire/firestore';
import { MatDialogRef } from '@angular/material/dialog';
import { RegisterService } from 'src/app/services/register.service';
import { UserDataService } from 'src/app/services/user-data.service';
import { AngularFireStorage } from "@angular/fire/storage";
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

@Component({
  selector: 'app-setup-dialog',
  templateUrl: './setup-dialog.component.html',
  styleUrls: ['./setup-dialog.component.css']
})
export class SetupDialogComponent implements OnInit {

  active:boolean=false;
  hide1:boolean=false;
  hide2:boolean=true;
  hide3:boolean=true;
  hide4:boolean=true;
  imageUrl:string;
  selectedImg;
  bannerUrl:string;
  bioDescription:string[]=[];
  currentUser:string;
  selectedBannerImg;
  setupForm: FormGroup;
  

  @ViewChild('bio') bio:ElementRef;

  constructor(public dialogRef: MatDialogRef<SetupDialogComponent>,
    public registerService:RegisterService, 
    public userService: UserDataService,
    public db: AngularFirestore,
    private storage: AngularFireStorage,
    public fb: FormBuilder,) {
    dialogRef.disableClose = true; 
   }

  ngOnInit(): void {
    // this.currentUser = localStorage.getItem('userId');
    this.registerService.getUser().subscribe(val => {
      val.map(u=>{
        if(u.payload.doc.id == this.userService.getUserId()){
          this.imageUrl=(u.payload.doc.get('profilePic'));
          this.bannerUrl=u.payload.doc.get('bannerPic');
        }
      })
    });
    this.createSetupForm();
    
  }

  createSetupForm(){
    console.log(this.bioDescription);
    this.setupForm=this.fb.group({
      bio:this.fb.control('',Validators.required),
    })
  }

  updateProfile(){
    console.log(this.setupForm.value.bio);
    this.db.collection('user').doc('/'+this.userService.getUserId()).update({bio:this.setupForm.value.bio})
    .then(()=>console.log('updated'))
    .catch(e => console.log(e))
  }

  @HostListener('document:click', ['$event'])
  checkClick(event: { target: any; }){
    if(this.bio.nativeElement.contains(event.target)){
      this.active=true;
    }
    else{
      this.active=false;
    }
  }

  showProfilePic(event:any){
    if(event.target.files && event.target.files[0]){
      const reader = new FileReader;
      reader.onload = (e:any) => this.imageUrl = e.target.result;
      reader.readAsDataURL(event.target.files[0]);
      this.selectedImg = event.target.files[0];
      const task = this.storage.upload(`user/${this.userService.getUserId()}/profilePic`, this.selectedImg);
      console.log(task);
    //   this.db.collection('user').doc('/'+this.userService.getUserId()).update({profilePic:task})
    // .then(()=>console.log('updated'))
    // .catch(e => console.log(e))
    }
  }

  showBannerPic(event:any){
    if(event.target.files && event.target.files[0]){
      const reader = new FileReader;
      reader.onload = (e:any) => this.bannerUrl = e.target.result;
      reader.readAsDataURL(event.target.files[0]);
      this.selectedBannerImg = event.target.files[0];
    //   this.db.collection('user').doc('/'+this.userService.getUserId()).update({bannerPic:this.selectedBannerImg.name})
    // .then(()=>console.log('updated'))
    // .catch(e => console.log(e))
    }
  }

  closeDialog(): void {
    this.dialogRef.close();
  }

  hidePicSection(){
    this.hide1=true;
    this.hide2=false;
  }
  hideHeaderSection(){
    this.hide1=false;
    this.hide2=true;
  }
  hideBioSection(){
    this.hide2=false;
    this.hide3=true;
  }
  hideFinalSection(){
    this.hide3=false;
    this.hide4=true;
  }
  showBioSection(){
    this.hide3=false;
    this.hide2=true;
  }
  showFinalSection(){
    this.hide4=false;
    this.hide3=true;
  }

}
