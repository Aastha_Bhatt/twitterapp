import { Component, OnInit } from '@angular/core';
import { AngularFireStorage } from '@angular/fire/storage';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatDialogRef } from '@angular/material/dialog';
import { RegisterService } from 'src/app/services/register.service';
import { TweetService } from 'src/app/services/tweet.service';
import { UserDataService } from 'src/app/services/user-data.service';
import { finalize } from 'rxjs/operators';

@Component({
  selector: 'app-tweet-dialog',
  templateUrl: './tweet-dialog.component.html',
  styleUrls: ['./tweet-dialog.component.css']
})
export class TweetDialogComponent implements OnInit {

  txtValue:string = '';
  enable:boolean=false;
  tweetForm:FormGroup;
  userName:string[]=[];
  userId:string[]=[];
  user:any[]=[];
  defaultPic:string[]=[];
  imageUrl:string='';
  selectedImg;

  constructor(public dialogRef: MatDialogRef<TweetDialogComponent>,
    public fb: FormBuilder,
    public registerService:RegisterService, 
    public userService: UserDataService,
    private storage: AngularFireStorage,
    public tweetService: TweetService) { }

  ngOnInit(): void {
    
    this.registerService.getUser().subscribe(val => {
      val.map(u=>{
        if(u.payload.doc.id == this.userService.getUserId()){
          this.userName.push(u.payload.doc.get('userName'));
          this.userId.push(u.payload.doc.get('userId'));
          this.defaultPic.push(u.payload.doc.get('profilePic'));
        }
      })
    });
    this.createTweet();
  }
  
  createTweet(){    
    this.tweetForm = this.fb.group({
      UID:this.fb.control(this.userService.getUserId(),Validators.required),
      description:this.fb.control('',[Validators.required, Validators.maxLength(280)]),
      likedByuserList:this.fb.array([]),
      likeCount:this.fb.control(0),
      reTweetByuserList:this.fb.array([]),
      commentCount:this.fb.control(0),
      retweetCount:this.fb.control(0),
      createdAt:this.fb.control(Date.now(),Validators.required),
      location:this.fb.control('Ahmedabad'),
      media:this.fb.control(this.imageUrl),
      isDeleted:this.fb.control(false),
      isPinned:this.fb.control(false),
    })
  }

  showDescriptionPic(event:any){
    if(event.target.files && event.target.files[0]){
      const reader = new FileReader;
      reader.onload = (e:any) => this.imageUrl = e.target.result;
      reader.readAsDataURL(event.target.files[0]);
      this.selectedImg = event.target.files[0];
      var mediaFilePath = `${this.userService.getUserId()}/description/${this.selectedImg.name.split('.').slice(0,-1).join('.')}_${new Date().getTime()}`;
      const fileRef = this.storage.ref(mediaFilePath);
      this.storage.upload(mediaFilePath,this.selectedImg)
      .snapshotChanges()
      .pipe(
        finalize(() => {
          fileRef.getDownloadURL().subscribe((url) => {
            this.imageUrl=(url);
          })
        })
      ).subscribe();
    }
    
  }

  addTweet(){
    if(this.enable){
      console.log(this.imageUrl);
      this.tweetForm.patchValue({
        media:this.imageUrl,
      })
      console.log(this.tweetForm.value.media);
      
      this.tweetService.createTweet(this.tweetForm.value);
      
      this.tweetForm.reset();
    }
    this.enable=false;
    this.imageUrl='';
  }

  closeDialog(): void {
    this.dialogRef.close();
  }

  onTextChange(value: string)
  {
    this.txtValue = value;
    if(this.txtValue.length)
    {
      this.enable=true;
    }
    else{
      this.enable=false;
    }
  }
}
