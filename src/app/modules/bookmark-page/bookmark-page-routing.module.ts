import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { DetailComponent } from '../detail/detail.component';
import { BookmarkPageComponent } from './bookmark-page.component';
import { BookmarksComponent } from './bookmarks/bookmarks.component';

const routes: Routes =[
    {
        path:'',
        component:BookmarkPageComponent,
        children:[
            {
                path:'',
                component:BookmarksComponent
            }
        ]
    },
    {
        path:'detail',
        component:DetailComponent
    }
]

@NgModule({
    imports: [RouterModule,RouterModule.forChild(routes)],
    exports: [RouterModule]
  })
  export class BookmarkPageRoutingModule { }