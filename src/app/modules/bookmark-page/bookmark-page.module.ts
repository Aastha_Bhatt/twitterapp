import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { FormsModule } from '@angular/forms';
import { BookmarkPageRoutingModule } from './bookmark-page-routing.module';

@NgModule({
    declarations: [
    ],
    imports: [
      // CommonModule,
      RouterModule,
      BookmarkPageRoutingModule,
      FormsModule,
    ]
  })
  export class BookmarkPageModule { }