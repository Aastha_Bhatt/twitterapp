import { Component, ElementRef, HostListener, Input, OnInit, ViewChild } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { Router } from '@angular/router';
import { SlideInOutAnimation } from 'src/app/animation';
import { QuoteDialogComponent } from 'src/app/dialog-box/quote-dialog/quote-dialog.component';
import { ReplyDialogComponent } from 'src/app/dialog-box/reply-dialog/reply-dialog.component';
import { RegisterService } from 'src/app/services/register.service';
import { TweetService } from 'src/app/services/tweet.service';
import { UserDataService } from 'src/app/services/user-data.service';

@Component({
  selector: 'app-tweet-card',
  templateUrl: './tweet-card.component.html',
  styleUrls: ['./tweet-card.component.css'],
  animations:[SlideInOutAnimation]
})
export class TweetCardComponent implements OnInit {

  @ViewChild('share') share:ElementRef;
  @ViewChild('retweet') retweet:ElementRef;
  @ViewChild('pinned') pinned:ElementRef;

  @Input('reply') reply:boolean;
  @Input('isTweet') isTweet:boolean;
  @Input('tweet') tweet:any[];
  @Input('parent') parent:boolean;
  @Input('child') child:boolean;

  show:boolean=false;
  txtValue:string = '';
  enable:boolean=false;
  retweetCount:number;
  likeCount:number;
  replyCount:number;
  showRetweetOption:boolean=false;
  showPinOption:boolean=false;
  retweeted:boolean=false;
  liked:boolean=false;
  pin:boolean=false;
  // defaultPic:string;
  userName:string;
  userId:string;
  profilePic:string;
  imageUrl:string='';
  descriptionImg:string;
  
  constructor(public replyDialog:MatDialog, 
    public quoteDialog:MatDialog, 
    private router:Router,
    public registerService:RegisterService, 
    public userService: UserDataService,
    ) { }

  ngOnInit(): void {
    this.likeCount=this.tweet['likes'];
    this.replyCount=this.tweet['comment'];
    this.retweetCount=this.tweet['retweet'];
    this.descriptionImg=this.tweet['descriptionImg']
    this.registerService.getUser().subscribe(val => {
      val.map(u=>{
        if(u.payload.doc.id == this.userService.getUserId()){
          this.userName=(u.payload.doc.get('userName'));
          this.userId=(u.payload.doc.get('userId'));
          this.profilePic=u.payload.doc.get('profilePic')
        }
      })
    });
  }

  checkProfile(){
    this.router.navigate(['/profile']);
  }

  openDetailPage(){
    this.router.navigate(['home/detail']);
  }

  openDialog(){    
    this.replyDialog.open(ReplyDialogComponent);
  }

  openQuoteDialog(){    
    this.quoteDialog.open(QuoteDialogComponent);
  }

  @HostListener('document:click', ['$event'])
  checkClick(event: { target: any; }){
    if(this.share.nativeElement.contains(event.target)){
      this.show=true;
    }
    else if(this.retweet.nativeElement.contains(event.target)){
      this.showRetweetOption=true;
    }
    else if(this.pinned.nativeElement.contains(event.target)){
      this.showPinOption=true;
    }
    else{
      this.show=false;
      this.showRetweetOption=false;
      this.showPinOption=false;
    }
  }

  onTextChange(value: string)
  {
    this.txtValue = value;
    if(this.txtValue.length)
    {
      this.enable=true;
    }
    else{
      this.enable=false;
    }
  }

  hideBox(){
    this.show=false;
  }

  hideRetweetBox(){
    this.showRetweetOption=false;
  }

  hidePinnedBox(){
    this.showPinOption=false;
  }

  increaseRetweet(){
    this.retweetCount++;
    this.retweeted=true;
  }

  decreaseRetweet(){
    this.retweetCount--;
    this.retweeted=false;
  }

  increaseLike(){
    if(this.liked)
    {
      this.likeCount--;
    }
    else{
      this.likeCount++;
    }
    this.liked=!this.liked;
  }

  showRetweetBox(){
    this.showRetweetOption=true;
  }

  showPinnedBox(){
    this.showPinOption=true;
  }

  pinTweet(){
    this.pin=true;
  }

  unpinTweet(){
    this.pin=false;
  }

}
