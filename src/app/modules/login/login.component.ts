import { Component, ElementRef, HostListener, OnInit, ViewChild } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatDialog } from '@angular/material/dialog';
import { Router } from '@angular/router';
import { SignupDialogComponent } from 'src/app/dialog-box/signup-dialog/signup-dialog.component';
import { AngularFireAuth } from '@angular/fire/auth';
import { RegisterService } from 'src/app/services/register.service';
import { UserDataService } from 'src/app/services/user-data.service';


@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  hide:boolean=true;
  active:boolean=false;
  activePsd:boolean=false;
  usernameValue:string='';
  passwordValue:string='';
  enable:boolean=false;
  user:any[];

  loginForm:FormGroup;

  @ViewChild('username') username:ElementRef;
  @ViewChild('password') password:ElementRef;

  constructor(public signupDialog:MatDialog,
     private router:Router,
     public fb: FormBuilder,
     public registerService:RegisterService,
     private auth: AngularFireAuth,
     public userService: UserDataService) { }

  ngOnInit(): void {
    this.createLoginForm();
  }

  createLoginForm(){
    this.loginForm = this.fb.group({
      userName:this.fb.control('', Validators.required),
      pswd:this.fb.control('', Validators.required),
    })
  }

  openHomePage(){
    this.registerService.getUser().subscribe(user =>{
      this.user = user.map(e => {
        return {
          id: e.payload.doc.id,
          email: e.payload.doc.get('email'),
          password: e.payload.doc.get('password')
        }
        
      })
      for(let i=0; i<this.user.length;i++){
        if(this.user[i].password == this.loginForm.value.pswd && this.user[i].email == this.loginForm.value.userName){
          this.userService.getCurrentUser(this.user[i].id);
          this.router.navigate(['home']);
        }
      }
      this.loginForm.reset();
    })
  }

  @HostListener('document:click', ['$event'])
  checkClick(event: { target: any; }){
    if(this.username.nativeElement.contains(event.target)){
      this.active=true;
    }
    else if(this.password.nativeElement.contains(event.target)){
      this.activePsd=true;
    }
    else{
      this.active=false;
      this.activePsd=false;
    }
  }

  openDialog(){    
    this.signupDialog.open(SignupDialogComponent);
  }

  onTextChange(value: string)
  {
    this.usernameValue = value;
    if(this.usernameValue.length && this.passwordValue.length)
    {
      this.enable=true;
    }
    else{
      this.enable=false;
    }
  }
  onPasswordChange(value: string)
  {
    this.passwordValue = value;
    if(this.usernameValue.length && this.passwordValue.length)
    {
      this.enable=true;
    }
    else{
      this.enable=false;
    }
  }

}
