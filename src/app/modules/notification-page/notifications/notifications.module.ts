import { NgModule } from '@angular/core';
import { NotificationsRoutingModule } from './notifications-routing.module';
import { RouterModule } from '@angular/router';
import { FormsModule } from '@angular/forms';

@NgModule({
  declarations: [
    // NotificationsComponent,
  ],
  imports: [
    // CommonModule,
    RouterModule,
    NotificationsRoutingModule,
    FormsModule,
  ]
})
export class NotificationsModule { }
