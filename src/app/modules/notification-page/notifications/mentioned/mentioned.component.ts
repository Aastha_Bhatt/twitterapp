import { Component, ElementRef, HostListener, OnInit, ViewChild } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { Router } from '@angular/router';
import { SlideInOutAnimation } from 'src/app/animation';

@Component({
  selector: 'app-mentioned',
  templateUrl: './mentioned.component.html',
  styleUrls: ['./mentioned.component.css'],
  animations:[SlideInOutAnimation]
})
export class MentionedComponent implements OnInit {

  constructor(private router:Router) { }

  ngOnInit(): void {
  }

  openDetailPage(){
    this.router.navigate(['home/detail']);
  }

}
