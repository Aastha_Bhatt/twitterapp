import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AllComponent } from './all/all.component';
import { MentionedComponent } from './mentioned/mentioned.component';

const routes: Routes = [
  {
    path:'',
    redirectTo:'all',
    pathMatch:'full'
  },
  { 
    path: 'all', 
    component: AllComponent,
  },
  {
    path:'mentions',
    component:MentionedComponent,
  }
];

@NgModule({
  imports: [RouterModule,RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class NotificationsRoutingModule { }
