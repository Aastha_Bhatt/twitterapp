import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { DetailComponent } from '../detail/detail.component';
import { NotificationPageComponent } from './notification-page.component';
import { NotificationsComponent } from './notifications/notifications.component';

const routes: Routes =[
    {
        path:'',
        component:NotificationPageComponent,
        children:[
            {
                path:'',
                component:NotificationsComponent,
                loadChildren: () => import('../../modules/notification-page/notifications/notifications.module').then(m => m.NotificationsModule) 
            },
            
        ]
    },
    {
        path:'detail',
        component:DetailComponent,
        loadChildren: () => import('../../modules/detail/detail.module').then(m => m.DetailModule) 
    },
]

@NgModule({
    imports: [RouterModule,RouterModule.forChild(routes)],
    exports: [RouterModule]
  })
  export class NotificationPageRoutingModule { }