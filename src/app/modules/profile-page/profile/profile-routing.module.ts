import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { LikesComponent } from './likes/likes.component';
import { MediaComponent } from './media/media.component';
import { TweetsRepliesComponent } from './tweets-replies/tweets-replies.component';
import { TweetsComponent } from './tweets/tweets.component';

const routes: Routes = [
  { 
    path: '', 
    redirectTo:'tweets',
    pathMatch:'full'
  },
  {
    path:'tweets',
    component:TweetsComponent
  },
  {
    path:'with_replies',
    component:TweetsRepliesComponent
  },
  {
    path:'media',
    component:MediaComponent
  },
  {
    path:'likes',
    component:LikesComponent
  },
  
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ProfileRoutingModule { }
