import { Component, OnDestroy, OnInit } from '@angular/core';
import { Location } from '@angular/common';
import { MatDialog } from '@angular/material/dialog';
import { SetupDialogComponent } from 'src/app/dialog-box/setup-dialog/setup-dialog.component';
import { ActivatedRoute, Router } from '@angular/router';
import { RegisterService } from 'src/app/services/register.service';
import { UserDataService } from 'src/app/services/user-data.service';
import { AngularFirestore } from '@angular/fire/firestore';
import { EditProfileComponent } from 'src/app/dialog-box/edit-profile/edit-profile.component';
import * as firebase from 'firebase';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.css']
})
export class ProfileComponent implements OnInit, OnDestroy {

  follow:boolean=false;
  user:any[];
  userName:string;
  userId:string;
  month:string;
  year:string;
  doj:string[]=[];
  tweetCount:number;
  profilePic:string;
  bannerPic:string;
  followingList:string[]=[];
  followData:any[]=[];
  currentUser;
  IsFollowing:boolean=false;
  hideSetUp:boolean=false;
  hideEdit:boolean=true;
  paramsSubscription : Subscription;
  
  constructor(private location: Location, 
    public setupDialog:MatDialog, 
    public editDialog:MatDialog, 
    private activatedRoute: ActivatedRoute,
    public registerService:RegisterService, 
    public userService: UserDataService,
    public db: AngularFirestore,
    public route: ActivatedRoute) { }

  ngOnInit(): void {
    console.log('profile');
    this.paramsSubscription = this.route.params.subscribe(params => {
      if(this.userService.getSearchUserId()){
        this.registerService.getUser().subscribe(val => {
          val.map(u=>{
            if(u.payload.doc.id == this.userService.getSearchUserId()){
              this.userName=u.payload.doc.get('userName');
              this.userId=u.payload.doc.get('userId');
              this.month=u.payload.doc.get('currentMonth');
              this.year=u.payload.doc.get('currentYear');
              this.profilePic=u.payload.doc.get('profilePic');
              this.bannerPic=u.payload.doc.get('bannerPic');
            }
            if(u.payload.doc.id == this.userService.getUserId()){
              for(let i=0; i<u.payload.doc.get('followingInt');i++){
                this.followingList=(u.payload.doc.get('followingList'));   
                this.IsFollowing=false;             
                if(this.userService.getSearchUserId() == this.followingList[i]){
                  this.IsFollowing=true;
                  this.follow=true;
                  break;
                }
              }
            }
          })
        });
        if(this.userService.getUserId() == this.userService.getSearchUserId()){
          this.follow=false;
        }
        else{
          this.follow=true;
        }
      }
      else{
        this.follow=false;
        this.registerService.getUser().subscribe(val => {
          val.map(u=>{
            if(u.payload.doc.id == this.userService.getUserId()){
              this.userName=u.payload.doc.get('userName');
              this.userId=u.payload.doc.get('userId');
              this.doj.push(u.payload.doc.get('doj'));
              this.profilePic=u.payload.doc.get('profilePic');
              this.bannerPic=u.payload.doc.get('bannerPic');
            }
          })
        });
      }
      this.currentUser = this.userService.getUserId();
    });
   
    
  }

  ngOnDestroy() {
    console.log("Component will be destroyed");
    this.paramsSubscription.unsubscribe();
  }

  addToFollowing(){
    this.followingList.push(this.userService.getSearchUserId());
    console.log(this.followingList);
    this.db.collection('user').doc('/'+this.userService.getUserId()).update({followingList:this.followingList,followingInt:this.followingList.length})
    .then(()=>console.log('updated'))
    .catch(e => console.log(e))
    this.IsFollowing=true;
  }

  removeFromFollowing(){
    console.log(this.followingList);
    this.followingList.forEach((element,index)=>{
      console.log(element);
    
      if(element == this.userService.getSearchUserId()) this.followingList.splice(index,1);
    });
    console.log(this.followingList);
  
  this.db.collection('user').doc('/'+this.userService.getUserId()).update({followingList:this.followingList,followingInt:this.followingList.length})
    .then(()=>console.log('updated'))
    .catch(e => console.log(e))
    this.IsFollowing=false;
  }

  goBack() {
    this.location.back();
  }

  openDialog(){    
    this.setupDialog.open(SetupDialogComponent);
    this.hideSetUp=false;
    this.hideEdit=true;
  }
  openEditDialog(){    
    this.editDialog.open(EditProfileComponent);
  }

}
