import { Component, ElementRef, EventEmitter, HostListener, OnInit, Output, ViewChild } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { SlideInOutAnimation } from 'src/app/animation';
import { UserDataService } from 'src/app/services/user-data.service';

@Component({
  selector: 'app-tweets',
  templateUrl: './tweets.component.html',
  styleUrls: ['./tweets.component.css'],
  animations: [SlideInOutAnimation]
})
export class TweetsComponent implements OnInit {

  follow:boolean=false;

  @Output() followBtn = new EventEmitter<boolean>();

  constructor(private router:Router, 
    private activatedRoute: ActivatedRoute,
    public userService: UserDataService) { }

  ngOnInit(): void {
  }

  sendFollowBtn() {
    this.followBtn.emit(this.follow); // emit username on click
  }

  openDetailPage(){
    this.router.navigate(['home/detail']);
  }

}
