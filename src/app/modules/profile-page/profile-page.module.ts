import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { FormsModule } from '@angular/forms';
import { ProfilePageRoutingModule } from './profile-page-routing.module';
import { ProfilePageComponent } from './profile-page.component';
import { ProfileComponent } from './profile/profile.component';
import { FollowlistComponent } from './followlist/followlist.component';

@NgModule({
    declarations: [
    //   ProfilePageComponent,
      // ProfileComponent,
      // FollowlistComponent
    ],
    imports: [
      // CommonModule,
      RouterModule,
      ProfilePageRoutingModule,
      FormsModule,
    ]
  })
  export class ProfilePageModule { }