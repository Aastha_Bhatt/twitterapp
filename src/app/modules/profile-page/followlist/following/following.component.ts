import { Component, HostListener, OnInit } from '@angular/core';
import { RegisterService } from 'src/app/services/register.service';
import { UserDataService } from 'src/app/services/user-data.service';

@Component({
  selector: 'app-following',
  templateUrl: './following.component.html',
  styleUrls: ['./following.component.css']
})
export class FollowingComponent implements OnInit {

  userName:string;
  userId:string;
  profilePic:string;
  followingList:string[]=[];
  followingInt:number;
  userInfo:string[]=[];

  constructor(public registerService:RegisterService, 
    public userService: UserDataService,) { }

  ngOnInit(): void {
    this.registerService.getUser().subscribe(val => {
      val.map(u => {
       if(u.payload.doc.id == this.userService.getUserId()){
         this.followingList.push(u.payload.doc.get('followingList'));
         this.followingInt=( u.payload.doc.get('followingInt'));
       }
      })
      console.log(this.followingInt);
      for(let i=0; i<this.followingInt;i++){
       console.log(this.followingList[i]);
       this.registerService.getUser().subscribe(val => {
         val.map(u => {
           if(u.payload.doc.id == this.followingList[i]){
            this.userInfo.push(
              this.userName=u.payload.doc.get('userName'),
              this.userId=u.payload.doc.get('userId'),
            )
           }
         })
       })
      }
    })
    
  }
  

}
