import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FollowlistComponent } from './followlist.component';
import { FollowlistRoutingModule } from './followlist-routing.module';
import { FollowersComponent } from './followers/followers.component';
import { FollowingComponent } from './following/following.component';
import { FollowBtnDirective } from 'src/app/directives/follow-btn.directive';

@NgModule({
    declarations: [
      // FollowlistComponent,
      FollowersComponent,
      FollowingComponent,
    ],
    imports: [
      CommonModule,
      FollowlistRoutingModule
    ]
  })
  export class FollowlistModule { }