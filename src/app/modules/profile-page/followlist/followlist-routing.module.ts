import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { FollowersComponent } from './followers/followers.component';
import { FollowingComponent } from './following/following.component';

const routes: Routes =[
    {
        path:'following',
        component:FollowingComponent,
        data: {animation: 'FollowingPage'}
    },
    {
        path:'followers',
        component:FollowersComponent,
        data: {animation: 'FollowersPage'}
    }
]

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
  })
  export class FollowlistRoutingModule { }