import { Component, ElementRef, HostListener, OnInit, ViewChild } from '@angular/core';
import { SlideInOutAnimation } from 'src/app/animation';
import { RegisterService } from 'src/app/services/register.service';
import { UserDataService } from 'src/app/services/user-data.service';
import { AngularFirestore } from '@angular/fire/firestore';
import { TweetService } from 'src/app/services/tweet.service';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { AngularFireStorage } from '@angular/fire/storage';
import { Observable } from 'rxjs';
import { finalize } from 'rxjs/operators';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css'],
  animations: [SlideInOutAnimation]
})
export class HomeComponent implements OnInit {

  show:boolean=false;
  txtValue:string = '';
  enable:boolean=false;
  isTweet:boolean=true;
  user:any[]=[];
  tweets:any[]=[];
  description:string;
  tweetForm:FormGroup;
  userName:string;
  userId:string[]=[];
  defaultPic:string[]=[];
  profilePic:string;
  currentUser:string;
  selectedImg;
  selectedFile: File ;
  fbs;
  downloadURL: Observable<string>;
  imageUrl:string='';

  constructor(public registerService:RegisterService, 
    public userService: UserDataService,
    public fireStore: AngularFirestore,
    public tweetService: TweetService,
    private storage: AngularFireStorage,
    public fb: FormBuilder,) { }

  ngOnInit(): void {
    this.tweetService.getTweet().subscribe(val => {
      this.tweets=[];
      val.map(u=>{
        if(u.payload.doc.get('UID') == this.userService.getUserId()){
          this.tweets.push({
            description:u.payload.doc.get('description'),
            descriptionImg:u.payload.doc.get('media'),
            likes:u.payload.doc.get('likeCount'),
            comment:u.payload.doc.get('commentCount'),
            retweet:u.payload.doc.get('retweetCount'),
          })
        }
      })
    });
    this.registerService.getUser().subscribe(val => {
      val.map(u=>{
        if(u.payload.doc.id == this.userService.getUserId()){
          this.profilePic=u.payload.doc.get('profilePic')
        }
      })
    });
    this.createTweet();
  }

  createTweet(){    
    this.tweetForm = this.fb.group({
      UID:this.fb.control(this.userService.getUserId(),Validators.required),
      description:this.fb.control('',[Validators.required, Validators.maxLength(280)]),
      likedByuserList:this.fb.array([]),
      likeCount:this.fb.control(0),
      reTweetByuserList:this.fb.array([]),
      commentCount:this.fb.control(0),
      retweetCount:this.fb.control(0),
      createdAt:this.fb.control(Date.now(),Validators.required),
      location:this.fb.control('Ahmedabad'),
      media:this.fb.control(this.imageUrl),
      isDeleted:this.fb.control(false),
      isPinned:this.fb.control(false),
    })
  }

  showDescriptionPic(event:any){
    if(event.target.files && event.target.files[0]){
      const reader = new FileReader;
      reader.onload = (e:any) => this.imageUrl = e.target.result;
      reader.readAsDataURL(event.target.files[0]);
      this.selectedImg = event.target.files[0];
      var mediaFilePath = `${this.userService.getUserId()}/description/${this.selectedImg.name.split('.').slice(0,-1).join('.')}_${new Date().getTime()}`;
      const fileRef = this.storage.ref(mediaFilePath);
      this.storage.upload(mediaFilePath,this.selectedImg)
      .snapshotChanges()
      .pipe(
        finalize(() => {
          fileRef.getDownloadURL().subscribe((url) => {
            this.imageUrl=(url);
          })
        })
      ).subscribe();
    }
    
  }
  
  addTweet(){
    if(this.enable){
      console.log(this.imageUrl);
      this.tweetForm.patchValue({
        media:this.imageUrl,
      })
      console.log(this.tweetForm.value.media);
      
      this.tweetService.createTweet(this.tweetForm.value);
      
      this.tweetForm.reset();
    }
    this.enable=false;
    this.imageUrl='';
  }

  onTextChange(value: string)
  {
    this.txtValue = value;
    if(this.txtValue.length)
    {
      this.enable=true;
    }
    else{
      this.enable=false;
    }
  }
}


