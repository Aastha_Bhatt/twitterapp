import { Component, ElementRef, HostListener, OnInit, ViewChild } from '@angular/core';
import { MatDialogRef } from '@angular/material/dialog';

@Component({
  selector: 'app-compose',
  templateUrl: './compose.component.html',
  styleUrls: ['./compose.component.css']
})
export class ComposeComponent implements OnInit {

  constructor(public dialogRef: MatDialogRef<ComposeComponent>) { }

  ngOnInit(): void {
  }

  ngAfterViewInit() {
    console.log("afterinit");
    setTimeout(() => {
      console.log(this.message.nativeElement.innerText);
    }, 9000);
  }

  txtValue:string = '';
  fill:boolean=false;
  status: boolean = false;
  hide:boolean=true;

  @ViewChild('message',{static: false}) message:ElementRef;

  @HostListener('document:click', ['$event'])
  checkClick(event: { target: any; }){
    if(this.message.nativeElement.contains(event.target)){
      this.status=true;
      this.hide=false;
    }
    else{
      this.status=false;
      this.hide=true;
    }
  }

  onTextChange(value: string)
  {
    this.txtValue = value;
    if(this.txtValue.length)
    {
      this.fill=true;
      this.hide=false;
    }
    else{
      this.fill=false;
      this.hide=false;
    }
  }

  clickEvent(){
    this.status = true;    
  }

  onNoClick(): void {
    this.dialogRef.close();
  }

}
