import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { MessagesRoutingModule } from './messages-routing.module';
import { MessagesComponent } from './messages.component';
import { SettingsComponent } from './settings/settings.component';
import { ComposeComponent } from './compose/compose.component';
import { RouterModule } from '@angular/router';


@NgModule({
  declarations: [
    // MessagesComponent,
    SettingsComponent,
    ComposeComponent
  ],
  imports: [
    CommonModule,
    RouterModule,
    MessagesRoutingModule,
    FormsModule,
  ]
})
export class MessagesModule { }
