import { Directive, HostBinding, HostListener, OnInit } from '@angular/core';

@Directive({
  selector: '[appFollowBtn]'
})
export class FollowBtnDirective implements OnInit {

  constructor() { }

  ngOnInit(): void {
    console.log('init');
  }

  @HostBinding('class.active')
  activeClass:boolean = false;

  @HostListener('mouseover')
  addUnfollow(){
    console.log('active');
    
    this.activeClass = true;
  }

  @HostListener('mouseleave')
  removeUnfollow(){
    this.activeClass = false;
  }

}
