import { Component, OnInit } from '@angular/core';
import { RouterOutlet } from '@angular/router';
import { SlideInOutAnimation } from 'src/app/animation';

@Component({
  selector: 'app-conversation',
  templateUrl: './conversation.component.html',
  styleUrls: ['./conversation.component.css'],
  animations:[SlideInOutAnimation]
})
export class ConversationComponent implements OnInit {

  opened:boolean=true

  constructor() { }

  ngOnInit(): void {
  }

  prepareRoute(outlet: RouterOutlet) {
    return outlet && outlet.activatedRouteData && outlet.activatedRouteData.animation;
  }

}
