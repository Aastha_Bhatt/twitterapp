import { Component, OnInit } from '@angular/core';
import { RouterOutlet } from '@angular/router';
import { SlideInOutAnimation } from 'src/app/animation';

@Component({
  selector: 'app-default',
  templateUrl: './default.component.html',
  styleUrls: ['./default.component.css'],
  animations:[SlideInOutAnimation]
})
export class DefaultComponent implements OnInit {

  opened:boolean=true

  constructor() { }

  ngOnInit(): void {
  }

  prepareRoute(outlet: RouterOutlet) {
    return outlet && outlet.activatedRouteData && outlet.activatedRouteData.animation;
  }

}
