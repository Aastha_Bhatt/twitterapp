import { Injectable } from '@angular/core';
import { AngularFirestore, AngularFirestoreCollection } from '@angular/fire/firestore';
import { AngularFireAuth } from '@angular/fire/auth';
import  auth  from 'firebase/app';
import { UserDataService } from './user-data.service';

@Injectable({
  providedIn: 'root'
})
export class RegisterService {

  constructor(public fireStore: AngularFirestore, 
    private auth: AngularFireAuth,
    public userService: UserDataService) { 
  }

  createUser(user){
    // const {email, password} = user;
    // this.auth.createUserWithEmailAndPassword(email,password).then(user =>{
    //   console.log(user.user?.uid);
    // });
    // this.fireStore.doc(`user/${this.userService.getSearchUserId}/followingList`).delete()
    return this.fireStore.collection('user').add(user);
  }

  getUser(){
    return this.fireStore.collection('user').snapshotChanges();
  }

}
