import { Injectable } from '@angular/core';
import { AngularFirestore } from '@angular/fire/firestore';

@Injectable({
  providedIn: 'root'
})
export class TweetService {

  constructor(public fireStore: AngularFirestore) { }

  createTweet(tweet){
    console.log(tweet);
    
    return this.fireStore.collection('tweet').add(tweet);
  }

  getTweet(){
    return this.fireStore.collection('tweet').snapshotChanges();
  }
}
