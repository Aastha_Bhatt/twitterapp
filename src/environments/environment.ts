// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebase:{
    apiKey: "AIzaSyAca567wX-UMLuG6sMteqOU3v6db4xcArc",
    authDomain: "twitterdatabase-fbfd2.firebaseapp.com",
    projectId: "twitterdatabase-fbfd2",
    storageBucket: "twitterdatabase-fbfd2.appspot.com",
    messagingSenderId: "111373624005",
    appId: "1:111373624005:web:1577eefe495f08fa0506ac"
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
